# allegro-recruitment

This app depends on data from github archive. It can be downloaded using download.sh script (the first argument is the path for the directory where the data is to be saved)

To run this app on a local instance of Spark I used the run.sh script.

The app accepts four arguments:
	1. The first one is the path for the directory with downloaded files (!!! DO NOT CHANGE NAMES GIVEN BY GITHUB !!!)
	2. The second one is the output path
	3. The third is the start date for data
	4. The last one is the end date for data
